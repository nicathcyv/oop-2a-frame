<?php

Class Blog_model extends CI_Model{


	public function getMenu()
	{
		$this->db->select('*');
		$this->db->from('menus');
		// $this->db->where('id', '1');
		return $this->db->get()->result_array();

	}


	public function getMeny(){
		$query = "SELECT * FROM menus order by id desc";

		$result = $this->db->query($query);

		return $result->result_array();
	}


	public function getBlog(){
		$this->db->select('*');
		$this->db->from('blog');
		// $this->db->where('id', '1');
		return $this->db->get()->result_array();	
	}

	public function getBlogById($id){
		$this->db->select('*');
		$this->db->from('blog');
		$this->db->where('id', $id);
		return $this->db->get()->result_array();	
	}


}