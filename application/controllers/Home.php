<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('blog_model');
		$this->load->helper('functions');
	}

	public function index(){
	
		$data['menus'] = $this->blog->getMeny();
		$data['blogPosts'] = $this->blog->getBlog();

		$data['subview'] = 'index.php';
		
		return $this->load->view('layout', $data);
	}

	public function blog($id){

		$this->load->model('blog_model', 'blog');
		$post = $this->blog->getBlogById($id);
		$this->load->view('details', $post);
	}	
}